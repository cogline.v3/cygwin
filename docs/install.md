# Install and maintain Cygwin

This guide was tested under Windows 10. It should be work with other [supported Windows Versions](https://www.cygwin.com/faq.html#faq.what.supported) too.

## Install Cygwin (without admin rights)

This documentation is based on [Oliver Doepner's IT blog](https://odoepner.wordpress.com/2014/01/10/install-and-maintain-cygwin-without-windows-admin-rights/). Thank you for your work Oliver.

1. Download [setup-x86_64.exe](http://cygwin.com/setup-x86_64.exe).
2. Open command line window (WIN+R and type `cmd`).
3. Go to the download directory, e. g.:
```PowerShellsh
cd %USERPROFILE%\Downloads
```
4. Run setup-x86_64.exe with the `--no-admin` option:
```PowerShellsh
setup-x86_64.exe --no-admin
```
5. During installation select the `wget` package.
6. After installation open a Cygwin Terminal via Windows desktop shortcut.
7. If you are behind an internet proxy (for example on a company environment) create `/etc/profile.d/proxy.sh` for internet access:
```sh
#!/usr/bin/env bash
 
export http_proxy=http://your.proxy.server:proxy_port
export https_proxy=http://your.proxy.server:proxy_port
export ftp_proxy=http://your.proxy.server:proxy_port
export no_proxy=localhost,.example.com
```
8. Now you need a file called `/usr/local/bin/cyg-get`. Downlaod it via
```sh
wget -O /usr/local/bin/cyg-get https://gitlab.com/cogline.v3/cygwin/raw/master/cyg-get?inline=false
```
Alternatively, you can also create the file yourself.
```sh
#!/usr/bin/env bash

# import proxy definition
[[ -r /etc/profile.d/proxy.sh ]] && . /etc/profile.d/proxy.sh

readonly SUCCESS=0
readonly ERROR=1
readonly E_WRONG_ARGS=2

get_current_version()
{
  cd /usr/local/bin

  [[ -e setup-x86_64.exe ]] && rm setup-x86_64.exe
  wget -q http://cygwin.com/setup-x86_64.exe
  chmod u+x setup-x86_64.exe
}

main()
{
  local all_parameters="$@"
  local params="--no-admin"
  local packages=

  while :; do
    case "$1" in
      install)
        params="${params} -q -P"
        shift
        # list of comma separated packages
        packages=$(echo $@|sed -e 's/\s\+/,/g')
        params="${params} ${packages}"
        ;;
      remove)
        params="${params} -q -x"
        shift
        packages=$(echo $@|sed -e 's/\s\+/,/g')
        params="${params} ${packages}"
        ;;
      update)
        params="${params} -q -g"
        get_current_version
        ;;
      # Unknonw option
      -?*)
        echo "unknown option: $1"
        exit $E_WRONG_ARGS
        ;;
      # Default case: If no more options then break out of the loop.
      *)
        get_current_version
        break
    esac
    shift
  done

  run /usr/local/bin/setup-x86_64.exe ${params}
  exit $SUCCESS
}

main "$@"
```
9. Make the script executable:
```sh
chmod ugo+x /usr/local/bin/cyg-get
```
10. On the Windows desktop create a copy of the Cygwin terminal shortcut, rename it _Cygwin Setup_.
11. Edit the shortcut target, replace:
```
mintty.exe -i /Cygwin-Terminal.ico -
```
with
```
mintty.exe -i /Cygwin-Terminal.ico /bin/bash -l -c 'cyg-get'
```
12. Now test it and execute the desktop shortcut or run cyg-get from the Cygwin command
prompt.

## Cygwin Package Management

With the created script `/usr/local/bin/cyg-get` you can do package management on command line like in a linux environment. Alternatively, you could also use the pure command-line tool [apt-cyg](https://github.com/transcode-open/apt-cyg "apt-cyg a Cygwin package manager").

### Install packages
```sh
cyg-get install package_1 package_2 ... package_n
```

### Remove packages
```sh
cyg-get remove package_1 package_2 ... package_n
```

### Update all packages

```sh
cyg-get update
```

!!! Note
    The `update` option will first download a new version of `setup-x86_64.exe` and after that all packages will be updated.

