# How to Use Cygwin on Windows

[Cygwin](https://www.cygwin.com/ "Cygwin Project") is:

* a large collection of GNU and Open Source tools which provide functionality similar to a Linux distribution on Windows.
* a DLL (cygwin1.dll) which provides substantial POSIX API functionality.

The Cygwin DLL currently works with all recent, commercially released x86 32 bit and 64 bit versions of Windows, starting with Windows Vista. For more information see the [FAQ](https://www.cygwin.com/faq.html#faq.what.supported "What versions of Windows are supported?").

* [Install and maintain Cygwin](/install/)
