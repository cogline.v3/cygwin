# How to Use Cygwin on Windows

[![Documentation Status](https://readthedocs.org/projects/cygwin/badge/?version=latest)](https://cygwin.readthedocs.io/en/latest/?badge=latest)

This is my collection of Cygwin How To's, see [cygwin.readthedocs.io](https://cygwin.readthedocs.io/) for details.

## Version

Release: 1.0.1

## License

GPLv3

## Author Information

This documentation was created in 2019 by Cogline.v3.
